package com.bluescope.bluescope.utils;

import android.content.Context;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by Three Hero on 08/09/2015.
 */
public class ConnectionManager {

    public static final String URL_ADD_CATEGORY = Config.makeUrlString(Config.suffix_add_category);
    public static final String URL_GET_CATEGORY = Config.makeUrlString(Config.suffix_get_category);
    public static final String URL_GET_CORPORATE = Config.makeUrlString(Config.suffix_get_corporate);
    public static final String URL_ADD_MERCHANT = Config.makeUrlString(Config.suffix_add_merchant);
    public static final String URL_ADD_CORPORATE = Config.makeUrlString(Config.suffix_add_corporate);
    public static final String URL_GET_LISTMERCHAT = Config.makeUrlString(Config.suffix_get_listMerchant);
    public static final String URL_GET_LISTOUTLET = Config.makeUrlString(Config.suffix_get_listOutlet);
    public static final String URL_GET_CHECK_USER = Config.makeUrlString(Config.suffix_get_check_user);
    public static final String URL_UPDATE_DATA = Config.makeUrlString(Config.suffix_update_data);
    public static final String URL_REGISTRASI_USER = Config.makeUrlString(Config.suffix_registrasi_user);
    public static final String URL_LOGIN_USER = Config.makeUrlString(Config.suffix_login_user);
    public static final String URL_CHEKING_VALIDATION = Config.makeUrlString(Config.suffix_checking_byId);
    public static final String URL_POST_POINT = Config.makeUrlString(Config.suffix_post_point);
    public static final String URL_GET_POINT = Config.makeUrlString(Config.suffix_get_point);


    private static String executeHttpClientPost(String requestURL, HashMap<String, String> params, Context context) throws IOException {
        URL url;
        String response = "";
        try {
            url = new URL(requestURL);

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(50000);
            conn.setConnectTimeout(50000);
            conn.setRequestMethod("POST");
            conn.setDoInput(true);
            conn.setDoOutput(true);
            conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");


            OutputStream os = conn.getOutputStream();
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(os, "UTF-8"));
            writer.write(getPostDataString(params));

            writer.flush();
            writer.close();
            os.close();
            int responseCode = conn.getResponseCode();

            if (responseCode == HttpsURLConnection.HTTP_OK) {
                String line;
                BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                while ((line = br.readLine()) != null) {
                    response += line;
                }
            } else {
                response = "";

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return response;
    }

    private static String executeHttpClientGet(String requestURL, Context context) throws IOException {
        URL url;
        String response = "";
        try {
            url = new URL(requestURL);

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            //conn.setReadTimeout(15000);
            //conn.setConnectTimeout(15000);
            conn.setRequestMethod("GET");
            //conn.setDoInput(true);
            //conn.setDoOutput(true);
            //conn.setRequestProperty("Content-Type","application/x-www-form-urlencoded");

            int responseCode = conn.getResponseCode();

            if (responseCode == HttpsURLConnection.HTTP_OK) {
                String line;
                BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                while ((line = br.readLine()) != null) {
                    response += line;
                }
            } else {
                response = "";

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return response;
    }

    private static String getPostDataString(HashMap<String, String> params) throws UnsupportedEncodingException {
        StringBuilder result = new StringBuilder();
        boolean first = true;
        for (Map.Entry<String, String> entry : params.entrySet()) {
            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
        }

        return result.toString();
    }

    public static String checkUser(String url, String email, String type, Context context) throws IOException {
        HashMap<String, String> params = new HashMap<>();
        params.put("email", email);
        params.put("type", type);
        return executeHttpClientPost(url, params, context);
    }

    public static String regUser(String url, String username, String email, String password, String name,
                                 String alamat, String register_type, String status, String birtdate,
                                 String telp, Context context) throws IOException {
        HashMap<String, String> params = new HashMap<>();
        params.put("username", username);
        params.put("email", email);
        params.put("password", password);
        params.put("address", alamat);
        params.put("register_type", register_type);
        params.put("name", name);
        params.put("status", status);
        params.put("birthdate", birtdate);
        params.put("id_corporate", "");
        params.put("id_member", String.valueOf(System.currentTimeMillis()));
        params.put("telp", telp);
        return executeHttpClientPost(url, params, context);
    }

    public static String loginUser(String url, String username, String password,
                                   Context context) throws IOException {
        HashMap<String, String> params = new HashMap<>();
        params.put("username", username);
        params.put("password", password);
        return executeHttpClientPost(url, params, context);
    }

    public static String validationCheck(String url, Context context) throws IOException {
        HashMap<String, String> params = new HashMap<>();
        return executeHttpClientPost(url, params, context);
    }

    public static String postPoint(String url, String id_merchant, String username,
                                   String email, String point, String amount,
                                   Context context) throws IOException {
        HashMap<String, String> params = new HashMap<>();
        params.put("id_member", "");
        params.put("id_merchant", id_merchant);
        params.put("username", username);
        params.put("email", email);
        params.put("point", point);
        params.put("amount", amount);
        return executeHttpClientPost(url, params, context);
    }


    public static String connectHttpGet(String url, Context context) throws IOException {
        return executeHttpClientGet(url, context);
    }


    //The params, is filter news Headline or No Headline
    public static String requestNews(String url, Context context) throws IOException {
        return connectHttpGet(url, context);
    }

    //The params, is filter news Headline or No Headline
    public static String requestGetWithoutParams(String url, Context context) throws IOException {
        return connectHttpGet(url, context);
    }
}
