package com.bluescope.bluescope.utils;

/**
 * Created by Three Hero on 19/08/2015.
 */
public class Config {
    public static final String URL_HTTP_STRING = "http://";
    public static final String URL_HTTPS_STRING = "https://";

    /*
   |-----------------------------------------------------------------------------------------------
   | URL server for this application to transfer data.
   |-----------------------------------------------------------------------------------------------
    */
    public static final String url_Server = "wawcard.com/api";
    public static final String suffix_add_category      = "categories";
    public static final String suffix_get_category      = "getAllCategories";
    public static final String suffix_get_corporate     = "getAllCorporate";
    public static final String suffix_get_listMerchant  = "getListMerchant";
    public static final String suffix_get_listOutlet    = "getListOutlet";
    public static final String suffix_add_merchant      = "merchant";
    public static final String suffix_add_corporate     = "corporate";
    public static final String suffix_get_check_user    = "checking";
    public static final String suffix_update_data       = "updated";
    public static final String suffix_registrasi_user   = "register";
    public static final String suffix_login_user        = "login";
    public static final String suffix_checking_byId     = "checkingById";
    public static final String suffix_post_point        = "point";
    public static final String suffix_get_point         = "getPoint";

    /*
   |-----------------------------------------------------------------------------------------------
   | Key for json
   |-----------------------------------------------------------------------------------------------
   */
    public static final String KEY_JSON_PROFILE = "json";

    /*
   |-----------------------------------------------------------------------------------------------
   | Key for imei
   |-----------------------------------------------------------------------------------------------
   */
    public static final String KEY_IMEI = "imei";


    /*
   |-----------------------------------------------------------------------------------------------
   | Database name
   |-----------------------------------------------------------------------------------------------
   */
    public static final String DATABASE_NAME = "wawcard_db";

    /*
    |-----------------------------------------------------------------------------------------------
    | Database version
    |-----------------------------------------------------------------------------------------------
    */
    public static final int DATABASE_VERSION = 1;

    /*
  |-----------------------------------------------------------------------------------------------
  | Database delay time
  |-----------------------------------------------------------------------------------------------
  */
    public static final int DB_CLOSE_DELAY_TIME = 5000;

    /*
   |-----------------------------------------------------------------------------------------------
   | Key for PutExtra
   |-----------------------------------------------------------------------------------------------
   */

    public static final String ID_CATEGORY = "KEY_ID_CATEGORY";
    public static final String NAME_CATEGORY = "KEY_NAME_CATEGORY";


    /*
    |-----------------------------------------------------------------------------------------------
    | Method for append URL and URI as API
    |-----------------------------------------------------------------------------------------------
    */
    public static String makeUrlString(String uri) {
        StringBuilder url = new StringBuilder(URL_HTTP_STRING);
        url.append(url_Server);
        url.append("/");
        url.append(uri);
        return url.toString();
    }

}

