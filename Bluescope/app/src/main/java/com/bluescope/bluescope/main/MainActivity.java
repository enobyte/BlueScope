package com.bluescope.bluescope.main;

import android.support.annotation.IdRes;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.TextView;

import com.bluescope.bluescope.R;
import com.bluescope.bluescope.adapter.MainTabAdapter;
import com.bluescope.bluescope.fragment.NewFeed;
import com.bluescope.bluescope.fragment.ProjectReg;
import com.bluescope.bluescope.fragment.RedeemPoint;
import com.bluescope.bluescope.fragment.information;
import com.bluescope.bluescope.fragment.Event;
import com.bluescope.bluescope.fragment.Merchant;
import com.bluescope.bluescope.utils.Utils;

public class MainActivity extends AppCompatActivity implements ViewPager.OnPageChangeListener {
    private TextView toolbarTitle;
    private TabLayout mTabLayout;
    private ViewPager mViewPager;
    private AppBarLayout appBarLayout;
    private Toolbar mToolbar;
    //private CollapsingToolbarLayout collapsingToolbar;
    // private NestedScrollView scrollView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        appBarLayout = (AppBarLayout) findViewById(R.id.main_appbar);
        mToolbar = (Toolbar) findViewById(R.id.main_toolbar);
        //collapsingToolbar = (CollapsingToolbarLayout) findViewById(R.id.main_collapsing_toolbar);
        mViewPager = (ViewPager) findViewById(R.id.main_viewpager);
        mTabLayout = (TabLayout) findViewById(R.id.main_tabs);
        toolbarTitle = (TextView) mToolbar.findViewById(R.id.toolbar_title);

        //scrollView = (NestedScrollView) findViewById (R.id.nested);
        //scrollView.setFillViewport (true);
        //scrollView.setNestedScrollingEnabled(true);

        //collapsingToolbar.setTitle(getString(R.string.info));
        //collapsingToolbar.setExpandedTitleTextAppearance(R.style.ExpandedAppBar);
        //collapsingToolbar.setCollapsedTitleTextAppearance(R.style.CollapsedAppBar);
        //collapsingToolbar.setCollapsedTitleTypeface(Utils.LatoHeavy(this));
        //collapsingToolbar.setExpandedTitleTypeface(Utils.LatoHeavy(this));
        toolbarTitle.setText(getString(R.string.info));
        mViewPager.setOffscreenPageLimit(6);

        setPager();

    }

    private void setPager() {
        setupViewPager(mViewPager);
        mTabLayout.setupWithViewPager(mViewPager);

        int[] icons = new int[]{R.drawable.selector_color_tab_info, R.drawable.ic_event,
                R.drawable.ic_project_reg, R.drawable.redeem, R.drawable.ic_bd_treat, R.drawable.ic_shooping};

        for (int i = 0; i < mTabLayout.getTabCount(); i++) {
            ImageView imageView = new ImageView(MainActivity.this);
            imageView.setImageResource(icons[i]);
            mTabLayout.getTabAt(i).setCustomView(imageView);
        }


        /*mTabLayout.getTabAt(0).setIcon(R.drawable.ic_information);
        mTabLayout.getTabAt(1).setIcon(R.drawable.ic_event);
        mTabLayout.getTabAt(2).setIcon(R.drawable.ic_shooping);
        mTabLayout.getTabAt(3).setIcon(R.drawable.ic_project_reg);
        mTabLayout.getTabAt(4).setIcon(R.drawable.redeem);
        mTabLayout.getTabAt(5).setIcon(R.drawable.ic_bd_treat);*/

        mViewPager.setOnPageChangeListener(this);

    }

    private void setupViewPager(ViewPager viewPager) {
        MainTabAdapter adapter = new MainTabAdapter(getSupportFragmentManager());
        adapter.addFragment(new information());
        adapter.addFragment(new Event());
        adapter.addFragment(new ProjectReg());
        adapter.addFragment(new RedeemPoint());
        adapter.addFragment(new NewFeed());
        adapter.addFragment(new Merchant());
        viewPager.setAdapter(adapter);
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        switch (position) {
            case 0:
            default:
                toolbarTitle.setText("Info");
                break;
            case 1:
                toolbarTitle.setText("Event");
                break;
            case 2:
                toolbarTitle.setText("Project Registration");
                break;
            case 3:
                toolbarTitle.setText("Redeem Point");
                break;
            case 4:
                toolbarTitle.setText("New Feed");
                break;
            case 5:
                toolbarTitle.setText("Merchant");
                break;
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        finish();
    }
}
