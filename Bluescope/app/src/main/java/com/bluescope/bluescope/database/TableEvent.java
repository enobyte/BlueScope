package com.bluescope.bluescope.database;


import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "Event")
public class TableEvent {

    public static final String TABLE_NAME = "Event";
    public static final String C_ID = "id";
    public static final String C_TIME = "time";
    public static final String C_TITLE = "title";
    public static final String C_DETAIL = "detail";
    public static final String C_PERSON = "person";
    public static final String C_ADDRESS = "address";

    @DatabaseField(generatedId = true)
    private int id;

    @DatabaseField
    private String time;
    @DatabaseField
    private String title;
    @DatabaseField
    private String detail;
    @DatabaseField
    private String person;
    @DatabaseField
    private String address;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPerson() {
        return person;
    }

    public void setPerson(String person) {
        this.person = person;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }


}