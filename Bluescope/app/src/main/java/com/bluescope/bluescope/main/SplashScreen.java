package com.bluescope.bluescope.main;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;

import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.widget.RelativeLayout;

import com.bluescope.bluescope.R;

import java.util.HashMap;
import java.util.Map;

public class SplashScreen extends AppCompatActivity {
    private static int SPLASH_TIME_OUT = 3000;
    //    private Utils utils;
    private int PERMISSION_ALL = 1;
    private RelativeLayout activity_splash_screen;
    //    private TableLoginAdapter adapter;
//    private List<TableLogin> list = new ArrayList<>();
    private String name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        activity_splash_screen = (RelativeLayout) findViewById(R.id.activity_splash_screen);
        //new DatabaseManager(this);
        //utils = new Utils(this);
        //setPermissionForM();
        displaySplashScreen();
    }


    private void displaySplashScreen() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(new Intent(SplashScreen.this, LoginActivity.class));
                finish();
            }
        }, SPLASH_TIME_OUT);
    }

    /**
     * Method for display Splash screen
     */
    /*private void displaySplashScreen() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                adapter = new TableLoginAdapter(SplashScreen.this);
                Long count = adapter.getDataCount();
                if (count > 0) {
                    try {
                        list = adapter.getAllData();
                        JSONObject object = new JSONObject(list.get(0).getValue());
                        if (object != null){
                            if (object.has("name")){
                                name = object.getString("name");
                            }else{
                                name = object.getString("username");
                            }

                        }

                        if (list.get(0).getUser_type().equalsIgnoreCase("u")) {
                            Intent intent = new Intent(SplashScreen.this, MainMenu.class);
                            intent.putExtra("name", name);
                            startActivity(intent);
                            finish();
                        } else if (list.get(0).getUser_type().equalsIgnoreCase("m")) {
                            Intent intent = new Intent(SplashScreen.this, List_user_visitor.class);
                            intent.putExtra("name", name);
                            startActivity(intent);
                            finish();
                        } else if (list.get(0).getUser_type().equalsIgnoreCase("a")) {
                            Intent intent = new Intent(SplashScreen.this, Main_frag_admin.class);
                            intent.putExtra("nama", name);
                            startActivity(intent);
                            finish();
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }

                } else {
                    Intent i = new Intent(SplashScreen.this, LoginActivity.class);
                    startActivity(i);
                    finish();
                }


            }
        }, SPLASH_TIME_OUT);
    }*/
    /*private void setPermissionForM() {
        String[] PERMISSIONS = {
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.CAMERA,
                Manifest.permission.READ_PHONE_STATE};
        if (!utils.hasPermissions(this, PERMISSIONS)) {
            ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_ALL);
        } else {
            displaySplashScreen();
        }
    }*/
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Map<String, Integer> perms = new HashMap<String, Integer>();
        // Initial
        perms.put(Manifest.permission.WRITE_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
        perms.put(Manifest.permission.READ_PHONE_STATE, PackageManager.PERMISSION_GRANTED);
        for (int i = 0; i < permissions.length; i++)
            perms.put(permissions[i], grantResults[i]);
        if (perms.get(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                && perms.get(Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED) {
            // All Permissions Granted
            displaySplashScreen();
        } else {
            // Permission Denied
            Snackbar.make(activity_splash_screen, "Some Permission is Denied", Snackbar.LENGTH_SHORT).show();
            //Toast.makeText(SplashScreen.this, "Some Permission is Denied", Toast.LENGTH_SHORT).show();
            displaySplashScreen();
        }

    }
}
