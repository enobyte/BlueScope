package com.bluescope.bluescope.adapter;

import android.content.Context;
import android.content.Intent;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bluescope.bluescope.R;
import com.bluescope.bluescope.database.TableCategory;
import com.bumptech.glide.Glide;

import java.util.List;
import java.util.Random;

/**
 * Created by admin on 12/4/2016.
 */

public class ListMerchantAdapter extends ArrayAdapter<TableCategory> {
    private final LayoutInflater mLayoutInflater;
    private final Random mRandom;
    private Context context;
    private static final SparseArray<Double> sPositionHeightRatios = new SparseArray<Double>();

    public ListMerchantAdapter(Context context, int textViewResourceId,
                               List<TableCategory> objects) {
        super(context, textViewResourceId, objects);
        this.mLayoutInflater = LayoutInflater.from(context);
        this.mRandom = new Random();
        this.context = context;
    }

    @Override
    public View getView(final int position, View convertView,
                        final ViewGroup parent) {

        ViewHolder vh;
        if (convertView == null) {
            convertView = mLayoutInflater.inflate(R.layout.item_list_merchant, parent, false);

            vh = new ViewHolder();
            vh.imgView = (ImageView) convertView.findViewById(R.id.imgView);
            vh.nameMerchant = (TextView) convertView.findViewById(R.id.nameMerchant);

            convertView.setTag(vh);
        } else {
            vh = (ViewHolder) convertView.getTag();
        }

        //double positionHeight = getPositionRatio(position);

        //vh.imgView.setHeightRatio(positionHeight);

        vh.nameMerchant.setText(getItem(position).getName());
        Glide.with(getContext()).load(getItem(position).getPhoto()).into(vh.imgView);
        /*vh.imgView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, Activity_listDetail_merchant.class);
                intent.putExtra(Config.ID_CATEGORY, getItem(position).getRowId());
                intent.putExtra(Config.NAME_CATEGORY, getItem(position).getName());
                context.startActivity(intent);
            }
        });*/
        return convertView;
    }

    static class ViewHolder {
        ImageView imgView;
        TextView nameMerchant;
    }

    private double getPositionRatio(final int position) {
        double ratio = sPositionHeightRatios.get(position, 0.0);
        if (ratio == 0) {
            ratio = getRandomHeightRatio();
            sPositionHeightRatios.append(position, ratio);
        }
        return ratio;
    }

    private double getRandomHeightRatio() {
        return (mRandom.nextDouble() / 2.0) + 1.0; // height will be 1.0 - 1.5
        // the width
    }
}