package com.bluescope.bluescope.database;


import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "category")
public class TableCategory {

    public static final String TABLE_NAME = "category";
    public static final String C_ID = "id";
    public static final String C_ROWID = "rowId";
    public static final String C_NAME = "name";
    public static final String C_PHOTO = "photo";
    public static final String C_STATUS = "status";

    @DatabaseField(generatedId = true)
    private int id;

    @DatabaseField
    private String rowId, name, photo, status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRowId() {
        return rowId;
    }

    public void setRowId(String rowId) {
        this.rowId = rowId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public TableCategory() {
    }
}