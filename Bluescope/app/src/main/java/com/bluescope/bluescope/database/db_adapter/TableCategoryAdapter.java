package com.bluescope.bluescope.database.db_adapter;

import android.content.Context;

import com.bluescope.bluescope.database.TableCategory;
import com.bluescope.bluescope.utils.DatabaseManager;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.Where;

import java.util.List;

/**
 * Created by Enobyte on 1/18/2017.
 */

public class TableCategoryAdapter {
    static private TableCategoryAdapter instance;
    private Dao dao;

    static public void init(Context ctx) {
        if (null == instance) {
            instance = new TableCategoryAdapter(ctx);
        }
    }

    static public TableCategoryAdapter getInstance() {
        return instance;
    }

    private DatabaseManager helper;

    public TableCategoryAdapter(Context ctx) {
        helper = new DatabaseManager(ctx);
    }

    private synchronized DatabaseManager getHelper() {
        return helper;
    }

    public List<TableCategory> getAllData() {
        List<TableCategory> tblsatu = null;
        try {
            tblsatu = getHelper().getTableCategoriesDAO()
                    .queryBuilder()
                    .orderBy(TableCategory.C_NAME, true)
                    .query();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tblsatu;
    }

    public List<TableCategory> getDataByCondition(String condition, String param) {
        List<TableCategory> tblsatu = null;
        int count = 0;
        try {
            dao = helper.getDao(TableCategory.class);
            QueryBuilder<TableCategory, Integer> queryBuilder = dao.queryBuilder();
            Where<TableCategory, Integer> where = queryBuilder.where();
            where.eq(condition, param);
            count++;

            if (count > 0) {
                tblsatu = queryBuilder.query();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tblsatu;
    }

    public void insertData(TableCategory tbl, String id, String name, String photo,
                           String status) {
        try {
            tbl.setRowId(id);
            tbl.setName(name);
            tbl.setPhoto(photo);
            tbl.setStatus(status);
            getHelper().getTableCategoriesDAO().create(tbl);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void deleteBy(Context context) {
        try {
            helper = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao = helper.getDao(TableCategory.class);
            DeleteBuilder<TableCategory, Integer> deleteBuilder = dao.deleteBuilder();
            deleteBuilder.delete();
        } catch (java.sql.SQLException e) {
            e.printStackTrace();
        }
    }
}
