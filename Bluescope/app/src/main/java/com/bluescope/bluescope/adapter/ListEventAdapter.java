package com.bluescope.bluescope.adapter;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.bluescope.bluescope.R;
import com.bluescope.bluescope.database.TableEvent;
import com.bluescope.bluescope.utils.VectorDrawableUtils;
import com.github.javiersantos.materialstyleddialogs.MaterialStyledDialog;
import com.github.vipulasri.timelineview.TimelineView;

import java.util.List;
import java.util.Random;

/**
 * Created by Enobyte on 2/26/2017.
 */

public class ListEventAdapter extends RecyclerView.Adapter<ListEventAdapter.Holder> {
    private List<TableEvent> list;
    private View itemLayout;
    private ViewGroup view;
    private Context context;

    public ListEventAdapter(Context context, List<TableEvent> list) {
        this.list = list;
        this.context = context;
    }


    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = parent;
        itemLayout = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_event, parent, false);
        Holder holder = new Holder(itemLayout);
        return holder;
    }

    @Override
    public int getItemViewType(int position) {
        return TimelineView.getTimeLineViewType(position, getItemCount());
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        final TableEvent item = list.get(position);

        int[] androidColors = context.getResources().getIntArray(R.array.androidcolors);
        int randomAndroidColor = androidColors[new Random().nextInt(androidColors.length)];
        holder.view.setBackgroundColor(randomAndroidColor);

        holder.item_time_event.setText(item.getTime());
        holder.item_title_event.setText(item.getTitle());
        holder.item_detail_event.setText(item.getDetail());
        holder.item_person_event.setText(item.getPerson());
        holder.item_address_event.setText(item.getAddress());
        holder.mTimelineView.setMarker(VectorDrawableUtils.getDrawable(context,
                R.drawable.ic_marker, R.color.colorPrimary));
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View customView = inflater.inflate(R.layout.item_detail_event, null);
                TextView tema = (TextView) customView.findViewById(R.id.tema);
                TextView title = (TextView) customView.findViewById(R.id.title);
                TextView date = (TextView) customView.findViewById(R.id.date);
                TextView address = (TextView) customView.findViewById(R.id.address);
                TextView note = (TextView) customView.findViewById(R.id.note);
                TextView cs = (TextView) customView.findViewById(R.id.ccontact_person);

                tema.setText(item.getTitle());
                title.setText(item.getDetail());
                date.setText(item.getTime());
                address.setText(item.getAddress());
                cs.setText(item.getPerson());

                new MaterialStyledDialog.Builder(context)
                        //.setTitle("Awesome!")
                        .setCustomView(customView)
                        .setPositiveText("Join")
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                Log.d("MaterialStyledDialogs", "Do something!");
                            }
                        })
                        .setNegativeText("No")
                        .onNegative(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

                            }
                        })
                        .setScrollable(true)


                        .show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    public class Holder extends RecyclerView.ViewHolder {
        private TextView item_time_event, item_title_event, item_detail_event, item_person_event,
                item_address_event;
        public TimelineView mTimelineView;
        private CardView view;

        public Holder(View itemView) {
            super(itemView);
            item_time_event = (TextView) itemView.findViewById(R.id.item_time);
            item_title_event = (TextView) itemView.findViewById(R.id.item_title_event);
            item_detail_event = (TextView) itemView.findViewById(R.id.item_detail_event);
            item_person_event = (TextView) itemView.findViewById(R.id.item_person);
            item_address_event = (TextView) itemView.findViewById(R.id.item_address_event);
            mTimelineView = (TimelineView) itemView.findViewById(R.id.time_marker);
            view = (CardView) itemView.findViewById(R.id.cv);
        }
    }

    private int getRandomMaterialColor(String typeColor) {
        int returnColor = Color.GRAY;
        int arrayId = context.getResources().getIdentifier("mdcolor_" + typeColor, "array", context.getPackageName());

        if (arrayId != 0) {
            TypedArray colors = context.getResources().obtainTypedArray(arrayId);
            int index = (int) (Math.random() * colors.length());
            returnColor = colors.getColor(index, Color.GRAY);
            colors.recycle();
        }
        return returnColor;
    }
}
