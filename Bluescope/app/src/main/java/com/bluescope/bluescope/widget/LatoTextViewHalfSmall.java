package com.bluescope.bluescope.widget;

import android.content.Context;
import android.util.AttributeSet;

/**
 * Created by Ari on 7/11/2016.
 */
public class LatoTextViewHalfSmall extends LatoTextView{
    final private int defaultFontSize = 11;
    public LatoTextViewHalfSmall(Context context, AttributeSet attributeSet){
        super(context, attributeSet);
        float scale = getResources().getConfiguration().fontScale;
        this.setTextSize(Math.round(defaultFontSize / scale));
    }
}
