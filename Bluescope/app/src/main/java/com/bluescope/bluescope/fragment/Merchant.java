package com.bluescope.bluescope.fragment;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ProgressBar;

import com.bluescope.bluescope.R;
import com.bluescope.bluescope.adapter.ListMerchantAdapter;
import com.bluescope.bluescope.database.TableCategory;
import com.bluescope.bluescope.database.db_adapter.TableCategoryAdapter;
import com.bluescope.bluescope.utils.ConnectionManager;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class Merchant extends Fragment {

    private ViewGroup viewGroup;
    private ProgressBar progressBar;
    private String response, id, name, photo, c_status, url;
    private boolean status;
    private ListMerchantAdapter adapterMerchat;
    private GridView mGridView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        viewGroup = (ViewGroup) inflater.inflate(R.layout.activity_page3, container, false);
        progressBar = (ProgressBar) viewGroup.findViewById(R.id.progress);
        mGridView = (GridView) viewGroup.findViewById(R.id.grid_view);
        return viewGroup;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        new AttempGetCategory().execute();
    }

    protected class AttempGetCategory extends AsyncTask<String, String, String> {
        @Override
        protected String doInBackground(String... strings) {
            try {
                url = ConnectionManager.URL_GET_CATEGORY;
                response = ConnectionManager.requestGetWithoutParams(url, getActivity());
                JSONObject jsonObject = new JSONObject(response);
                status = jsonObject.getBoolean("status");
                if (status) {
                    TableCategoryAdapter adapter = new TableCategoryAdapter(getActivity());
                    adapter.deleteBy(getActivity());
                }
                JSONArray data = new JSONArray(jsonObject.getString("data"));
                if (data != null) {
                    TableCategoryAdapter db = new TableCategoryAdapter(getActivity());
                    for (int i = 0; i < data.length(); i++) {
                        id = data.getJSONObject(i).getString("id");
                        name = data.getJSONObject(i).getString("name");
                        photo = data.getJSONObject(i).getString("photo");
                        c_status = data.getJSONObject(i).getString("status");
                        db.insertData(new TableCategory(), id, name, photo, c_status);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressBar.setVisibility(View.GONE);
            showList();
        }

        private void showList() {
            List<TableCategory> listData = new ArrayList<TableCategory>();
            TableCategoryAdapter adapter = new TableCategoryAdapter(getActivity());
            listData = adapter.getAllData();
            adapterMerchat = new ListMerchantAdapter(getActivity(), android.R.layout.simple_list_item_1, listData);
            mGridView.setAdapter(adapterMerchat);
            //mGridView.setOnItemClickListener(MainActivity.this);
        }
    }
}
