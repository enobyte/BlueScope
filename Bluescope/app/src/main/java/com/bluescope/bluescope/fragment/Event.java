package com.bluescope.bluescope.fragment;

import android.os.AsyncTask;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bluescope.bluescope.R;
import com.bluescope.bluescope.adapter.ListEventAdapter;
import com.bluescope.bluescope.adapter.ListMerchantAdapter;
import com.bluescope.bluescope.database.TableCategory;
import com.bluescope.bluescope.database.TableEvent;
import com.bluescope.bluescope.database.db_adapter.TableCategoryAdapter;
import com.bluescope.bluescope.database.db_adapter.TableEventAdapter;

import java.util.ArrayList;
import java.util.List;

public class Event extends Fragment {
    private ViewGroup viewGroup;
    private RecyclerView recyclerView;
    private TableEventAdapter adapter;
    private ListEventAdapter adapterMerchat;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        viewGroup = (ViewGroup) inflater.inflate(R.layout.activity_page2, container, false);
        recyclerView = (RecyclerView) viewGroup.findViewById(R.id.list_event);
        adapter = new TableEventAdapter(getActivity());
        new AttemptLoadData().execute();
        return viewGroup;
    }

    protected class AttemptLoadData extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... strings) {
            adapter.deleteBy(getActivity());
            adapter.insertData(new TableEvent(), "12:04 AM", "Gatthering", "Acara Peresmian Bendera",
                    "Larry Page", "Jakarta");
            adapter.insertData(new TableEvent(), "12:09 AM", "Meeting", "Meeting Launching",
                    "Mark Zuck", "Jakarta");
            adapter.insertData(new TableEvent(), "06:09 AM", "Gladi Resik", "Pementasan Lomba",
                    "Bill Gates", "Bali");
            adapter.insertData(new TableEvent(), "12:09 AM", "Meeting", "Meeting Launching",
                    "Mark Zuck", "Jakarta");
            adapter.insertData(new TableEvent(), "12:09 AM", "Meeting", "Meeting Launching",
                    "Mark Zuck", "Jakarta");
            adapter.insertData(new TableEvent(), "12:09 AM", "Meeting", "Meeting Launching",
                    "Mark Zuck", "Jakarta");
            adapter.insertData(new TableEvent(), "12:09 AM", "Meeting", "Meeting Launching",
                    "Mark Zuck", "Jakarta");
            adapter.insertData(new TableEvent(), "12:09 AM", "Meeting", "Meeting Launching",
                    "Mark Zuck", "Jakarta");
            adapter.insertData(new TableEvent(), "12:09 AM", "Meeting", "Meeting Launching",
                    "Mark Zuck", "Jakarta");
            adapter.insertData(new TableEvent(), "12:09 AM", "Meeting", "Meeting Launching",
                    "Mark Zuck", "Jakarta");
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            showList();
        }
    }

    private void showList() {
        List<TableEvent> listData = new ArrayList<TableEvent>();
        listData = adapter.getAllData();
        LinearLayoutManager layoutParams = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutParams);
        adapterMerchat = new ListEventAdapter(getActivity(), listData);
        recyclerView.setAdapter(adapterMerchat);
        //mGridView.setOnItemClickListener(MainActivity.this);
    }

}
