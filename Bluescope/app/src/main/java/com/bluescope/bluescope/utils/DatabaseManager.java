package com.bluescope.bluescope.utils;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.bluescope.bluescope.database.TableCategory;
import com.bluescope.bluescope.database.TableEvent;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;

/**
 * Created by admin on 6/10/2016.
 */
public class DatabaseManager extends OrmLiteSqliteOpenHelper {

    private static final String DATABASE_PATH = "/data/data/com.bluescope.bluescope/databases/";
    private Dao<TableCategory, Integer> tableCategoriesDAO = null;
    private Dao<TableEvent, Integer> tableEventsDAO = null;
    /*private Dao<TableCorporate, Integer> tableCorporateDAO = null;
    private Dao<TableMerchant, Integer> tableMerchantsDAO = null;
    private Dao<TableLogin, Integer> tableLoginDAO = null;*/

    public DatabaseManager(Context context) {
        super(context, DATABASE_PATH + Config.DATABASE_NAME, null, Config.DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase, ConnectionSource connectionSource) {
        try {
            TableUtils.createTable(connectionSource, TableCategory.class);
            TableUtils.createTable(connectionSource, TableEvent.class);
            /*TableUtils.createTable(connectionSource, TableMerchant.class);
            TableUtils.createTable(connectionSource, TableLogin.class);*/
            Log.d("db", "Can't create database");
        } catch (SQLException e) {
            Log.e("this", "Can't create database", e);
            throw new RuntimeException(e);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, ConnectionSource connectionSource, int oldVersion, int newVersion) {
        try {
            TableUtils.dropTable(connectionSource, TableCategory.class, true);
            TableUtils.dropTable(connectionSource, TableEvent.class, true);
            /*TableUtils.dropTable(connectionSource, TableMerchant.class, true);
            TableUtils.dropTable(connectionSource, TableLogin.class, true);*/
            onCreate(sqLiteDatabase, connectionSource);
        } catch (SQLException e) {
            Log.e("this", "Can't create database", e);
            throw new RuntimeException(e);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Dao<TableCategory, Integer> getTableCategoriesDAO() {
        if (null == tableCategoriesDAO) {
            try {
                tableCategoriesDAO = getDao(TableCategory.class);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return tableCategoriesDAO;
    }

    public Dao<TableEvent, Integer> getTableEventDAO() {
        if (null == tableEventsDAO) {
            try {
                tableEventsDAO = getDao(TableEvent.class);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return tableEventsDAO;
    }

    /*public Dao<TableMerchant, Integer> getTableMerchantsDAO() {
        if (null == tableMerchantsDAO) {
            try {
                tableMerchantsDAO = getDao(TableMerchant.class);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return tableMerchantsDAO;
    }*/

    /*public Dao<TableLogin, Integer> getTableLoginDAO() {
        if (null == tableLoginDAO) {
            try {
                tableLoginDAO = getDao(TableLogin.class);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return tableLoginDAO;
    }*/


    /**
     * Close the database connections and clear any cached tableDAO.
     */
    @Override
    public void close() {
        super.close();
        tableCategoriesDAO = null;
        tableEventsDAO = null;
        /*tableMerchantsDAO = null;
        tableLoginDAO = null;*/

    }
}

