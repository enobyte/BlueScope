package com.bluescope.bluescope.utils;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.telecom.Call;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.widget.Toast;

import com.bluescope.bluescope.R;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;


import static android.R.style.Theme;

/**
 * Created by chan on 18/10/2016.
 * Project IyadenApp
 * Founder by Ari Wibowo & Candra Sukma
 */
public class Utils {
    private static double longitude, latitude;

    public static final String regEx =  "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

    public static void setLocation(double lng, double lat) {
        longitude = lng;
        latitude = lat;
    }

    public static double getLongitude() {
        return longitude;
    }

    public static double getLatitude() {
        return latitude;
    }

    public static void showToast(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }

    public static String getDeviceName() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            return capitalize(model);
        }
        return capitalize(manufacturer) + " " + model;
    }

    private static String capitalize(String str) {
        if (TextUtils.isEmpty(str)) {
            return str;
        }
        char[] arr = str.toCharArray();
        boolean capitalizeNext = true;
        String phrase = "";
        for (char c : arr) {
            if (capitalizeNext && Character.isLetter(c)) {
                phrase += Character.toUpperCase(c);
                capitalizeNext = false;
                continue;
            } else if (Character.isWhitespace(c)) {
                capitalizeNext = true;
            }
            phrase += c;
        }
        return phrase;
    }

    public static String getCurrentTimeStamp() {
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        return String.valueOf(timestamp.getTime());
    }

    public static String getCurrentTime() {
        SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        Date now = new Date();
        String strDate = sdfDate.format(now);
        return strDate;
    }

    public static String formatTimeStamp(String timeStamp) {
        Timestamp stamp = new Timestamp(Long.parseLong(timeStamp));
        Date date = new Date(stamp.getTime());
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        String strTime = format.format(date);
        return strTime;
    }

    public static String formatTimeStampForFirst(String timeStamp) {
        Timestamp stamp = new Timestamp(Long.parseLong(timeStamp));
        Date date = new Date(stamp.getTime());
        SimpleDateFormat format = new SimpleDateFormat("HH:mm");
        String strTime = format.format(date);
        return strTime;
    }

    public static String formatMonth(String timeStamp) {
        String newDateString;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        Date d = null;
        try {
            d = sdf.parse(timeStamp);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        sdf.applyPattern("MMM dd hh:mm");
        newDateString = sdf.format(d);

        return newDateString;
    }

    public static Date formatStringToDate(String timeStamp) {
        DateFormat df = new SimpleDateFormat("MM-dd-yyyy");
        Date startDate = null;
        try {
            startDate = df.parse(timeStamp);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return startDate;
    }

    /*public static String formatToOnlyTime(String first) {
        String newDateString = "";
        if (first.length() > 0) {
            //Timestamp timestamp = new Timestamp(System.currentTimeMillis());
            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
            newDateString = sdf.format(first).toString();
        }
        return newDateString;
    }*/

    public static String formatToOnlyTime(String first) {
        String newDateString;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        Date d = null;
        try {
            d = sdf.parse(first);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        sdf.applyPattern("HH:mm");
        newDateString = sdf.format(d);

        return newDateString;
    }

    public static Typeface LatoBlack(Context mContext) {
        Typeface font = Typeface.createFromAsset(mContext.getResources().getAssets(), "fonts/Lato-Black.ttf");
        return font;
    }

    public static Typeface LatoBold(Context mContext) {
        Typeface font = Typeface.createFromAsset(mContext.getResources().getAssets(), "fonts/Lato-Bold.ttf");
        return font;
    }

    public static Typeface LatoHeavy(Context mContext) {
        Typeface font = Typeface.createFromAsset(mContext.getResources().getAssets(), "fonts/Lato-Heavy.ttf");
        return font;
    }

    public static Typeface LatoMedium(Context mContext) {
        Typeface font = Typeface.createFromAsset(mContext.getResources().getAssets(), "fonts/Lato-Medium.ttf");
        return font;
    }

    public static Typeface LatoThin(Context mContext) {
        Typeface font = Typeface.createFromAsset(mContext.getResources().getAssets(), "fonts/Lato-Thin.ttf");
        return font;
    }

    public static Typeface LatoLight(Context mContext) {
        Typeface font = Typeface.createFromAsset(mContext.getResources().getAssets(), "fonts/Lato-Light.ttf");
        return font;
    }

    public static Typeface LatoRegular(Context mContext) {
        Typeface font = Typeface.createFromAsset(mContext.getResources().getAssets(), "fonts/Lato-Regular.ttf");
        return font;
    }
}
