package com.bluescope.bluescope.widget;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

/**
 * Created by Ari on 7/11/2016.
 */
public class LatoTextViewLarge extends LatoTextView {
    final private int defaultFontSize = 20;

    public LatoTextViewLarge(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.setTypeface(Typeface.createFromAsset(context.getAssets(), "fonts/Lato-Black.ttf"));
        float scale = getResources().getConfiguration().fontScale;
        this.setTextSize(Math.round(defaultFontSize / scale));
    }
}
