package com.bluescope.bluescope.fragment;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import com.bluescope.bluescope.R;

public class NewFeed extends Fragment {
    private ViewGroup viewGroup;
    private WebView webview;
    private ProgressBar progressBar;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        viewGroup = (ViewGroup) inflater.inflate(R.layout.activity_bd_threat, container, false);
        webview = (WebView) viewGroup.findViewById(R.id.wv);
        progressBar = (ProgressBar) viewGroup.findViewById(R.id.progress);
        initView();
        return viewGroup;
    }

    private void initView() {

        WebSettings settings = webview.getSettings();
        settings.setJavaScriptEnabled(true);
        webview.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);

        webview.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);

        webview.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }

            public void onPageFinished(WebView view, String url) {
                webview.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.GONE);
            }

            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                webview.setVisibility(View.INVISIBLE);
                progressBar.setVisibility(View.GONE);
            }
        });
        webview.loadUrl("http://www.bluescope.co.id/bluescope-indonesia-id/");
    }
}
