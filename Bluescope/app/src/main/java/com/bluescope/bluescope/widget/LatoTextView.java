package com.bluescope.bluescope.widget;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by Ari on 7/11/2016.
 */
public class LatoTextView extends TextView {
    final private int defaultFontSize = 16;

    public LatoTextView(Context context, AttributeSet attributeSet){
        super(context, attributeSet);
        this.setTypeface(Typeface.createFromAsset(context.getAssets(), "fonts/Lato-Regular.ttf"));
        float scale = getResources().getConfiguration().fontScale;
        this.setTextSize(Math.round(defaultFontSize/scale));
    }
}
