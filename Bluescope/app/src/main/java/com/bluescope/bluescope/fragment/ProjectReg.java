package com.bluescope.bluescope.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bluescope.bluescope.R;
import com.bluescope.bluescope.widget.MultiSpinner;

import java.util.ArrayList;
import java.util.List;

public class ProjectReg extends Fragment {
    private ViewGroup viewGroup;
    private MultiSpinner multiSpinner;
    private List<String> items = new ArrayList<String>();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        viewGroup = (ViewGroup) inflater.inflate(R.layout.activity_project_reg, container, false);

        return viewGroup;
    }
}
