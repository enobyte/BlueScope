package com.bluescope.bluescope.database.db_adapter;

import android.content.Context;

import com.bluescope.bluescope.database.TableCategory;
import com.bluescope.bluescope.database.TableEvent;
import com.bluescope.bluescope.utils.DatabaseManager;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;

import java.util.List;

/**
 * Created by Enobyte on 2/26/2017.
 */

public class TableEventAdapter {
    static private TableEventAdapter instance;
    private Dao dao;

    static public void init(Context ctx) {
        if (null == instance) {
            instance = new TableEventAdapter(ctx);
        }
    }

    static public TableEventAdapter getInstance() {
        return instance;
    }

    private DatabaseManager helper;

    public TableEventAdapter(Context ctx) {
        helper = new DatabaseManager(ctx);
    }

    private synchronized DatabaseManager getHelper() {
        return helper;
    }

    public List<TableEvent> getAllData() {
        List<TableEvent> tblsatu = null;
        try {
            tblsatu = getHelper().getTableEventDAO()
                    .queryBuilder()
                    .orderBy(TableEvent.C_TIME, true)
                    .query();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tblsatu;
    }

    public void insertData(TableEvent tbl, String time, String title,
                           String detail, String person, String address) {
        try {
            tbl.setTime(time);
            tbl.setTitle(title);
            tbl.setDetail(detail);
            tbl.setPerson(person);
            tbl.setAddress(address);
            getHelper().getTableEventDAO().create(tbl);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void deleteBy(Context context) {
        try {
            helper = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao = helper.getDao(TableEvent.class);
            DeleteBuilder<TableEvent, Integer> deleteBuilder = dao.deleteBuilder();
            deleteBuilder.delete();
        } catch (java.sql.SQLException e) {
            e.printStackTrace();
        }
    }
}
